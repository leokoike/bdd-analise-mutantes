import platform
import os
import django

so = platform.system().lower()

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vendas.settings")
django.setup()

from django.core.management import call_command

if so == 'windows':
    os.system('python.exe manage.py makemigrations servicos')
    os.system('python.exe manage.py makemigrations loja')

    os.system('python.exe manage.py migrate servicos')
    os.system('python.exe manage.py migrate loja')
    os.system('python.exe manage.py migrate')

    os.system('python.exe manage.py runserver')

elif so == 'linux':
    os.system('python3 manage.py makemigrations servicos')
    os.system('python3 manage.py makemigrations loja')

    os.system('python3 manage.py migrate servicos')
    os.system('python3 manage.py migrate loja')
    os.system('python3 manage.py migrate')

    os.system('python3 manage.py runserver')
else:
    print("Sistema Operacional não identificado, inicie manualmente")

